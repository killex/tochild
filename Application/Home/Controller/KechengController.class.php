<?php
namespace Home\Controller;
use Think\Controller;
class KechengController extends Controller {
    public function index(){

        $this->display();
    }

    public function addbase(){
        $this->kechengbases = M('kechengbase')->select();
        // dump($this->kechengbases);
        $this->display();
    }
    public function runsave(){
        header("Content-type: text/html; charset=utf-8");
        $data = $_POST;
        $data["enable"] = 1;
        // dump($data);
        $result = M('kechengbase')->add($data);
        if($result){
            $this->success('操作成功',U('Home/kecheng/addbase'));
        }
        else{
            $this->error('操作失败');
        }
    }
}