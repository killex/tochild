/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : tochild

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-07-20 12:50:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `knownumrecord`
-- ----------------------------
DROP TABLE IF EXISTS `knownumrecord`;
CREATE TABLE `knownumrecord` (
  `uuid` varchar(32) DEFAULT NULL COMMENT 'login表唯一id',
  `success` int(11) DEFAULT NULL COMMENT '成功次数',
  `fail` int(11) DEFAULT NULL COMMENT '失败次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of knownumrecord
-- ----------------------------

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) NOT NULL COMMENT '唯一id',
  `name` varchar(50) NOT NULL COMMENT '登录名',
  `pass` varchar(50) NOT NULL COMMENT '登录密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
